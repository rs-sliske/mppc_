var Vue = require('vue');
window.VueStrap = require('vue-strap/dist/vue-strap.min.js');

var VueRouter = require('vue-router');
Vue.use(VueRouter);
var router = new VueRouter();

Vue.use(require('vue-resource'));

Vue.filter('limitBy', 
	function(data, arg){
		return data.splice(0, arg);
	});


Vue.component('modal',  VueStrap.modal);
Vue.component('carousel',  VueStrap.carousel);

var vue = Vue.extend({

	data(){
		return {
			store: store,
			modals: {
				login: false,
				register: false,
			},
			offset: {
				top : 70,
			},
		};
	},

	components: {
		'affix': VueStrap.affix,

		AlertList : require('./components/AlertList.vue'),
		LoginForm : require('./components/LoginForm.vue'),		
		NavBar : require('./components/NavBar.vue'),
	},

	directives: {
		Ajax: require('./directives/ajax.js'),
	},

	http:{
		headers:{
			'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
		},
	},

	ready(){
		this.$http.get('api/products').then(function(data){
			window.store.products = data.data;
		});
		this.$http.get('api/cart').then(function(data){
			window.store.cart = data.data;
		});
	},
});


router.map({
    '/': {
        component: require('./pages/HomePage.vue')
    },
    '/cart': {
        component: require('./pages/CartPage.vue'),
        subRoutes:{
        	'/summary' :{
        		component: require('./pages/cart/Summary.vue'),
        		step: 0,
        	},
        	'/shipping' :{
        		component: require('./pages/cart/Shipping.vue'),
        		step: 1,
        	},
        	'/billing' :{
        		component: require('./pages/NotFound.vue'),
        		step: 2,
        	},
        	'/confirm' :{
        		component: require('./pages/NotFound.vue'),
        		step: 3,
        	},
        	'/place' :{
        		component: require('./pages/NotFound.vue'),
        		step: 4,
        	},
        	'/*': {
        		component: require('./pages/NotFound.vue')
        	},     	
        },
    },
    '/products/:slug':{
    	name:'product',
		component : require('./pages/ProductPage.vue')
	},
    '/products': {
        component: require('./pages/ProductListPage.vue'),
    },
    '*':{
    	component: require('./pages/NotFound.vue')
    }
})

router.start(vue, 'body')