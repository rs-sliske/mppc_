export default {
	bind(){
		$(this.el).on('submit', this.onSubmit.bind(this));
	},
	update(){

	},
	onSubmit(e){
		e.preventDefault();
		this.vm
			.$http[this.getRequestType()](this.el.action, this.getFormData())
			.then(this.onComplete.bind(this));
	},
	getRequestType(){
		var method = this.el.querySelector('input[name="_method"]');
		return (method ? method.value : this.el.method).toLowerCase();
	},
	onComplete(data){
		alert('form sent');
	},

	getFormData() {
        var serializedData = $(this.el).serializeArray();
        var objectData = {};
        $.each(serializedData, function() {
            if (objectData[this.name] !== undefined) {
                if (!objectData[this.name].push) {
                    objectData[this.name] = [objectData[this.name]];
                }
                objectData[this.name].push(this.value || '');
            } else {
                objectData[this.name] = this.value || '';
            }
        });
        return objectData;
    },
};