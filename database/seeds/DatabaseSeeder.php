<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(MPPC\Models\Brand::class, 50)->create();
        factory(MPPC\Models\Category::class, 50)->create();
        factory(MPPC\Models\Description::class, 50)->create();
        factory(MPPC\Models\Price::class, 50)->create();
        factory(MPPC\Models\Product::class, 250)->create();
        factory(MPPC\Models\Tag::class, 50)->create();
        factory(MPPC\Models\User::class, 50)->create();
    }
}
