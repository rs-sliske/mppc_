<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(MPPC\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),        
        'api_token' => str_random(60),
    ];
});

$factory->define(MPPC\Models\Brand::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description_id' => rand(0,50),
    ];
});

$factory->define(MPPC\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description_id' => rand(0,50),
    ];
});

$factory->define(MPPC\Models\Description::class, function (Faker\Generator $faker) {
    return [
        'short_description' => $faker->text,
        'full_description' => $faker->text,
    ];
});

$factory->define(MPPC\Models\Price::class, function (Faker\Generator $faker) {
    return [
        'price' => rand(),
        'product_id' => rand(0,50),
    ];
});

$factory->define(MPPC\Models\Product::class, function (Faker\Generator $faker) {
    return [
    	'name' => $faker->word,
        'brand_id' => rand(0,50),
        'description_id' => rand(0,50),
        'slug' => $faker->slug,
        'price' => rand(),
        'stock' => rand(0, 10),
    ];
});

$factory->define(MPPC\Models\Tag::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});