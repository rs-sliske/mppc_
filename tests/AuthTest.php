<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory as TestDummy;

class AuthTest extends TestCase
{
    /** @test */
    public function it_can_display_registration_page()
    {
        $this->visit('register')
        	->see('email')
        	->see('password');
    }

    /** @test */
    public function it_can_register_a_user()
    {
    	$user = TestDummy::build('MPPC\Models\User');
        $this->visit('register')
        	->type($user->name, '#name')
        	->type($user->email, '#email')
        	->type('password', '#password')
        	->type('password', '#password_confirmation')
        	->press('Register')
        	->seePageIs('/');
    }

    /** @test */
    public function it_tells_user_if_passwords_dont_match()
    {
    	$user = TestDummy::build('MPPC\Models\User');
        $this->visit('register')
        	->type($user->name, '#name')
        	->type($user->email, '#email')
        	->type('password', '#password')
        	->type('PASSWORD', '#password_confirmation')
        	->press('Register')
        	->see('The password confirmation does not match');

    }

    /** @test */
    public function it_shows_login_page(){
    	$this->visit('login')
        	->see('email')
        	->see('password');
    }
}
