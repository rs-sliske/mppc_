<?php 

namespace MPPC;

use MPPC\Contracts\StorageInterface;

use MPPC\Models\Product;

class Cart {

	protected $storage;

	public function __construct(StorageInterface $storage){
		$this->storage = $storage;
	}

	public function add(Product $product, int $quantity = 1){
		$current = 0;
		if($this->has($product)){
			$current = (int) $this->storage->get("$product->id.quantity", 0);
		}
		return $this->update($product, $quantity + $current);
	}

	public function update(Product $product, int $quantity){
		// if(! $product->hasStock()){

		// }

		if($quantity === 0){
			$this->remove($product);
			return 0;
		}

		$this->storage->set($product->id, [
				'product_id' => (int) $product->id,
				'quantity' => (int) $quantity,
			]);
	}

	public function remove(Product $product){
		$this->storage->unset($product->id);
	}

	public function has(Product $product){
		return $this->storage->exists($product->id);
	}

	public function empty(){
		$this->storage->clear();
	}

	public function all(){
		$ids = [];
		$items = [];

		foreach($this->storage->all() as $product){
			$ids[] = $product['product_id'];
		}

		$products = Product::find($ids);

		foreach($products as $product){
			$data = [
				'product_id' => $product->id,
				'product_slug' => $product->slug,
				'quantity' => $this->storage->get("$product->id.quantity"),
				'stock' => $product->stock,
			];
			
			$items[$product->slug] = $data;
		}

		return $items;
	}

	public function itemCount(){
		return $this->storage->count();
	}

}