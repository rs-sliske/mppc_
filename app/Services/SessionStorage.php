<?php 

namespace MPPC\Services;

use MPPC\Contracts\StorageInterface;
use Countable;

use Session;


class SessionStorage implements StorageInterface, Countable
{
	protected $bucket;

	function __construct($bucket = 'default')
	{
		if(!Session::has($bucket))
			Session::put($bucket, []);	

		$this->bucket = $bucket;
	}

	public function get($index, $default = null){	
		return Session::get("$this->bucket.$index", $default);

	}

	public function set($index, $value){
		Session::put("$this->bucket.$index", $value);
	}

	public function all(){
		return Session::get($this->bucket, []);
	}

	public function exists($index){
		return Session::has("$this->bucket.$index");
	}

	public function unset($index){
		Session::forget("$this->bucket.$index");
	}

	public function clear(){
		Session::put($this->bucket, []);
	}

	public function delete(){
		Session::forget($this->bucket);
	}

	public function count(){
		return count($this->all());
	}
}