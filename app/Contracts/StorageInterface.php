<?php

namespace MPPC\Contracts;

interface StorageInterface {

	public function get($index, $default = null);
	public function set($index, $value);
	public function all();
	public function exists($index);
	public function unset($index);
	public function clear();
	public function delete();

}