<?php

namespace MPPC\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MPPC\Traits\RecordsChanges;

class Tag extends Model
{
	use SoftDeletes, RecordsChanges;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
    	'created_at', 
    	'updated_at', 
    	'deleted_at'
    ];

}
