<?php

namespace MPPC\Models;

use Illuminate\Database\Eloquent\Model;
use MPPC\Traits\RecordsChanges;

class ProductInput extends Model
{
    use RecordsChanges;

    protected $table = "input_product";

    public function product(){
    	return $this->belongsTo('MPPC\Models\Product');
    }

    public function input(){
    	return $this->belongsTo('MPPC\Models\Input');
    }
}
