<?php

namespace MPPC\Models;

use Illuminate\Database\Eloquent\Model;
use MPPC\Traits\RecordsChanges;

class Component extends Model
{
    use RecordsChanges;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'computer_id', 'product_input_id', 'connected_id', 'product_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    public static function make(Computer $computer, Product $product, ProductInput $input = null, Component $parent = null){
    	$component = new Component;
    	$component->computer_id = $computer->id;
    	$component->product_id = $product->id;
    	if(! $input){
    		//TODO:work out input if none is set
    		foreach($component in $computer->components){
    			
    		}
    	}
    	if(! $parent){
    		//TODO:work out parent if none is set
    	}
    	$component->product_input_id = $input->id;
    	$component->connected_id = $parent->id;
    	$component->save();    	
    	return $component;
    }

    public function computer(){
    	return $this->belongsTo('MPPC\Models\Computer');
    }
    
    public function parentComponent(){
    	return $this->belongsTo('MPPC/Models/Component', 'id', 'connected_id');
    }

    public function product(){
    	return $this->hasOne('MPPC/Models/Product');
    }

}
