<?php

namespace MPPC\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MPPC\Traits\RecordsChanges;

class Computer extends Model
{
   	use SoftDeletes, RecordsChanges;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function components(){
        return $this->hasMany('MPPC\Models\Component');
    }
}
