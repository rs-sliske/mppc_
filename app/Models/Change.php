<?php

namespace MPPC\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Change extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'subject_id',
        'subject_type',
        'event_name',
        'user_id',
        'before',
        'after',
    ];

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function subject(){
    	return $this->morphTo();
    }
}
