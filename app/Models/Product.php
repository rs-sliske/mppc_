<?php

namespace MPPC\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MPPC\Traits\RecordsChanges;

class Product extends Model
{
	use SoftDeletes, RecordsChanges;

    protected $fillable = [
        'name', 
        'brand_id', 
        'description_id',
        'slug',
        'price',
        'stock',
    ];

    protected $dates = [
    	'created_at', 
    	'updated_at', 
    	'deleted_at',
    ];
    
    public function brand(){
    	return $this->belongsTo('MPPC\Models\Brand');
    }

    public function description(){
    	return $this->belongsTo('MPPC\Models\Description');
    }

    public function categories(){
    	return $this->belongsToMany('MPPC\Models\Category')->withTimestamps();
    }

    public function tags(){
    	return $this->belongsToMany('MPPC\Models\Tag')->withTimestamps();
    }

    public function inputs(){
        return $this->belongsToMany('MPPC\Models\Input');
    }

    public function productInputs(){
        return $this->hasMany('MPPC\Models\ProductInput');
    }

    public function format(){
        return [
            'name' => $this['name'],
            'slug' => $this['slug'],
            'brand' => $this['brand']['name'],
            'price' => $this['price'],
            'stock' => $this['stock'],
            'description' => [
                'short' => $this['description']['short_description'],
                'full' => $this['description']['full_description'],
            ],
        ];        
    }
}
