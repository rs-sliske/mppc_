<?php

namespace MPPC\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MPPC\Traits\RecordsChanges;

class Input extends Model
{
    use SoftDeletes, RecordsChanges;

    protected $fillable = [
        'name', 
    ];

    protected $dates = [
    	'created_at', 
    	'updated_at', 
    	'deleted_at',
    ];

    public function foundOn(){
    	return $this->belongsToMany('MPPC\Models\Product');
    }

    public function productInputs(){
        return $this->hasMany('MPPC\Models\ProductInput');
    }
}
