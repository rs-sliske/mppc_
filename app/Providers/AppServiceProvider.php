<?php

namespace MPPC\Providers;

use Illuminate\Support\ServiceProvider;

use MPPC\Contracts\StorageInterface;
use MPPC\Services\SessionStorage;
use MPPC\Cart;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
        }

        $this->app->bind(StorageInterface::class, function(){
            return new SessionStorage('cart');
        });
    }
}
