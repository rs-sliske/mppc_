<?php

namespace MPPC\Http\Controllers\api;

use Illuminate\Http\Request;

use MPPC\Http\Requests;
use MPPC\Http\Controllers\Controller;
use MPPC\Cart;
use MPPC\Models\Product;

class CartController extends Controller
{
	protected $cart;

    public function __construct(Cart $cart){
    	$this->cart = $cart;

    	// $this->middleware('web');
    }

    public function add(Request $request){
    	$product = Product::whereSlug($request->input('product_slug'))->firstOrFail();
    	$this->cart->add($product, $request->input('quantity', 1));
    	return response()->json($this->cart->all());
    }

    public function get(){
    	return response()->json($this->cart->all());
    }

    public function update(Request $request){
   		switch($request->input('update')){
            case 'quantity':
                $product = Product::whereSlug($request->input('product_slug'))->firstOrFail();
                $this->cart->update($product, $request->input('quantity', 1));
                return response()->json($this->cart->all());
        }
        

   		return response()->json($this->cart->all()); 	
    }
}
