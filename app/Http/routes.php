<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => 'web'], function(){
	Route::auth();

	Route::get('/', function () {
	    return view('layouts.app');	
	});

	Route::get('/home', 'HomeController@index');
});

Route::group(['prefix' => 'api/', 'namespace' => 'api', 'middleware' => 'web'], function(){
	Route::resource('products', 'ProductController');
	Route::auth();

	Route::group(['middleware' => 'auth'], function(){
		Route::get('user/{user}', function(MPPC\Models\User $user){
			return response()->json($user);
		});
	});

	Route::post('cart', 'CartController@add');
	Route::get('cart', 'CartController@get');
	Route::patch('cart', 'CartController@update');	
});







// Route::group(['prefix' => 'api/', 'namespace' => 'api'], function(){
// 	Route::resource('products', 'ProductController');
// });
